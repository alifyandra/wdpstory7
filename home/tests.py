from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .models import Message
from .views import index, confirm
from .apps import HomeConfig

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/',follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_using_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_confirm(self):
        found = resolve('/confirm/')
        self.assertEqual(found.func, confirm)

    def test_index_contains_question(self):
        response = Client().get('/',follow=True)
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, how are you?", response_content)

    def test_using_index_template(self):
        response = Client().get('/',follow=True)
        self.assertTemplateUsed(response, 'home/index.html')

    def test_apps(self):
        self.assertEqual(HomeConfig.name, 'home')
        self.assertEqual(apps.get_app_config('home').name, 'home')

    def test_model_can_create_new_status(self):
        status = Message.objects.create(status='Status')

        counting_all_status = Message.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_form_validation_for_filled_items(self) :
        response = self.client.post('/confirm/', data = {'name':'Alco', 'status':'Not Good'})
        count_all_user = Message.objects.all().count()
        self.assertEqual(count_all_user, 1)

        self.assertEqual(response.status_code, 302)

        new_response = self.client.get('/')
        new_response_content = new_response.content.decode('utf-8')
        self.assertIn('Alco', new_response_content)
        self.assertIn('Not Good', new_response_content)



    