from django.db import models

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length=28)
    status = models.CharField(max_length=230, blank=True)
    time = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.name