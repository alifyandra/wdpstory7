from django import forms
from .models import Message

class POSTMessage(forms.ModelForm):
    class Meta:
        model = Message
        fields = (
            'name',
            'status',
        )