from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index, name="home-index"),
    path('confirm/', views.confirm, name='home-confirm')
]