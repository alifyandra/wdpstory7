from django.shortcuts import render, redirect
from .forms import *
from .models import Message
from django.utils import timezone
# Create your views here.
def index(request):
    if request.method == "POST":
        form = POSTMessage(request.POST)
        if form.is_valid():
            message = Message()
            message.name = form.cleaned_data['name']
            message.status = form.cleaned_data['status']
            response = {'name': message.name, 'status': message.status} 
            return render(request, 'home/confirm.html', response)
    message_count = Message.objects.count()
    form = POSTMessage()
    message = Message.objects.all().order_by('-time')
    response = {'message' : message, 'form': form, 'title':'Story 7','count':message_count}
    return render(request, 'home/index.html', response)

def confirm(request):
    if request.method == "POST":
        form = POSTMessage(request.POST)
        if form.is_valid():
            message = Message()
            message.name = form.cleaned_data['name']
            message.status = form.cleaned_data['status']
            message.save()
            return redirect('home-index')
    return render(request, 'home/confirm.html')
