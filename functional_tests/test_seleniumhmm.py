from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from django.test import TestCase, Client, LiveServerTestCase

class TestProject(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.browser.quit()
    
    def test_functional(self):
        self.browser.get(self.live_server_url)

        #Take content of the page
        response_content = self.browser.page_source

        #Check content of the page
        self.assertIn('Name', response_content)
        self.assertIn('Status', response_content)

        #Input and submit
        self.browser.find_element_by_name('name').send_keys('AlcoJoki')
        self.browser.find_element_by_name('status').send_keys('Status')
        
        #Take value in responses
        name = self.browser.find_element_by_id('inputName').get_attribute('value')
        status = self.browser.find_element_by_id('inputStatus').get_attribute('value')
        
        #Check User Input
        self.assertIn('AlcoJoki', name)
        self.assertIn('Status', status) 
        self.browser.find_element_by_name('submit-button').click()
        
        #Confirm the input
        self.browser.find_element_by_name('confirmation').click()

        #Change color
        self.browser.find_element_by_class_name('color-button').click()

        #Take color value
        target_color = self.browser.find_element_by_id('target').value_of_css_property('color')
        
        #Check change color (Red)
        self.assertEqual('rgba(255, 0, 0, 1)',target_color)